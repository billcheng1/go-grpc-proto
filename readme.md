
# Proto
To generate go files based on protobuf, run the following command

```bash
protoc --go_out=plugins=grpc:. --go_opt=paths=source_relative hello.proto
```
